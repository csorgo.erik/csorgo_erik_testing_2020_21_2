﻿using NUnit.Framework;
using OE_PAGE_OBJECT_BEADANDO.Models;
using OE_PAGE_OBJECT_BEADANDO.Pages;
using System;
using System.Collections;
using System.Linq;
using System.Xml.Linq;

namespace OE_PAGE_OBJECT_BEADANDO
{
    //Töki u. 7, 2071 Páty
    //button[aria-label = 'Bolognai pizza (24cm),  1 399 Ft - Tedd a kosárba']
    class BeadandoPageObjectTest : TestBase
    {
        [Test, TestCaseSource("SearchData")]
        public void SearchRestaurantTest(string location, string orderType)
        {

            SearchRestaurantModel model = new SearchRestaurantModel() { Location = location, OrderType = (OrderType)int.Parse(orderType) };
            var resultWidget = new SearchRestaurantPage(Driver)
                .Navigate(Driver)
                .GetSearchRestaurantWidget()
                .SearchRestaurant(model)
                .GetSearchResultWidget();

            Assert.IsTrue(resultWidget.CheckLocation(model.Location) && resultWidget.CheckOrderType(model.OrderType));
        }

        [Test, TestCaseSource("LoginData")]
        public void LoginTest(string name, string email, string password)
        {
            LoginModel model = new LoginModel() { Name = name, Email = email, Password = password };
            var resultWidget = new LoginPage(Driver)
                .Navigate(Driver)
                .GetLoginWidget()
                .ClickLogin(model)
                .GetLoginResultWidget();

            Assert.IsTrue(resultWidget.IsNameVisible(model.Name) && resultWidget.IsTokenCreated());

        }

        [Test, TestCaseSource("OrderData")]
        public void OrderPizzaTest(string name, string cartName)
        {
            var result = new OrderPizzaPage(Driver)
                .Navigate(Driver)
                .GetOrderPizzaWidget()
                .ClickOnPizza(name)
                .CheckCart(cartName);

            Assert.IsTrue(result);
        }

        static IEnumerable LoginData()
        {
            var doc = XElement.Load(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory) + "//loginData.xml");
            return
                from element in doc.Descendants("testData")
                let name = element.Attribute("name").Value
                let email = element.Attribute("email").Value
                let password = element.Attribute("password").Value
                select new object[] { name, email, password };
        }

        static IEnumerable OrderData()
        {
            var doc = XElement.Load(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory) + "//orderData.xml");
            return
                from element in doc.Descendants("testData")
                let name = element.Attribute("name").Value
                let cartName = element.Attribute("cartName").Value
                select new object[] { name, cartName };
        }

        static IEnumerable SearchData()
        {
            var doc = XElement.Load(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory) + "//searchData.xml");
            return
                from element in doc.Descendants("testData")
                let location = element.Attribute("location").Value
                let orderType = element.Attribute("orderType").Value
                select new object[] { location, orderType };
        }
    }

    
}
