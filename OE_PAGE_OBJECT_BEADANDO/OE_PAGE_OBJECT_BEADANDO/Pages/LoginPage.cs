﻿using OE_PAGE_OBJECT_BEADANDO.Widgets;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OE_PAGE_OBJECT_BEADANDO.Pages
{
    // refresh_token van akkor be vagyunk lépve
    //url: https://www.netpincer.hu/login/new?step=login
    class LoginPage : BasePage
    {
        public LoginPage(IWebDriver webDriver) : base(webDriver)
        {
        }

        public LoginPage Navigate(IWebDriver webDriver)
        {
            webDriver.Url = "https://www.netpincer.hu/login/new?step=login";
            webDriver.Manage().Window.Size = new System.Drawing.Size(1900, 1000);
            return new LoginPage(webDriver);
        }

        public LoginWidget GetLoginWidget()
        {
            return new LoginWidget(Driver);
        }
    }
}
