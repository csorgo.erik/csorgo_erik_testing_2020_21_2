﻿using OE_PAGE_OBJECT_BEADANDO.Widgets;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OE_PAGE_OBJECT_BEADANDO.Pages
{
    class SearchResultPage : BasePage
    {
        public SearchResultPage(IWebDriver webDriver) : base(webDriver)
        {
        }

        public SearchResultWidget GetSearchResultWidget()
        {
            return new SearchResultWidget(Driver);
        }
    }
}
