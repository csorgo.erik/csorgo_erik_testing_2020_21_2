﻿using OE_PAGE_OBJECT_BEADANDO.Widgets;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OE_PAGE_OBJECT_BEADANDO.Pages
{
    class OrderPizzaPage : BasePage
    {
        public OrderPizzaPage(IWebDriver webDriver) : base(webDriver)
        {
        }

        public OrderPizzaPage Navigate(IWebDriver webDriver)
        {
            webDriver.Url = "https://www.netpincer.hu/restaurant/new/bkgc/15-perc-pizza-by-pizza-king";
            webDriver.Manage().Window.Size = new System.Drawing.Size(1400, 1000);

            var LocationButton = webDriver.FindElement(By.CssSelector("button[data-testid = 'dialogue-confirm-cta']"));
            LocationButton.Click();

            var LocationInput = webDriver.FindElement(By.CssSelector("input[data-testid = 'restaurants-search-form__input']"));
            LocationInput.SendKeys("Kerepesi út 2-4, 1087 Budapest");

            var LocationSubmit = webDriver.FindElement(By.CssSelector("button[data-testid = 'location-search-go-icon']"));
            LocationSubmit.Click();

            return new OrderPizzaPage(webDriver);
        }

        public OrderPizzaWidget GetOrderPizzaWidget()
        {
            return new OrderPizzaWidget(Driver);
        }
    }
}
