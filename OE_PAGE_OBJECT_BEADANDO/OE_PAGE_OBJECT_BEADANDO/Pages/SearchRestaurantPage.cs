﻿using OE_PAGE_OBJECT_BEADANDO.Widgets;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OE_PAGE_OBJECT_BEADANDO.Pages
{
    class SearchRestaurantPage : BasePage
    {
        public SearchRestaurantPage(IWebDriver webDriver) : base(webDriver)
        {
        }

        public SearchRestaurantPage Navigate(IWebDriver webDriver)
        {
            webDriver.Url = "https://www.netpincer.hu/";
            webDriver.Manage().Window.Size = new System.Drawing.Size(1900,1000);
            var cookies = webDriver.FindElement(By.CssSelector("button[class = 'button js-ripple']"));
            cookies.Click();
            return new SearchRestaurantPage(webDriver);
        }

        public SearchRestaurantWidget GetSearchRestaurantWidget()
        {
            return new SearchRestaurantWidget(Driver);
        }
    }
}
