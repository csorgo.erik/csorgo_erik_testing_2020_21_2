﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OE_PAGE_OBJECT_BEADANDO.Models
{
    class SearchRestaurantModel
    {
        public string Location { get; set; }
        public OrderType OrderType { get; set; }
    }

    public enum OrderType
    {
        Delivery,
        Pickup
    }
}
