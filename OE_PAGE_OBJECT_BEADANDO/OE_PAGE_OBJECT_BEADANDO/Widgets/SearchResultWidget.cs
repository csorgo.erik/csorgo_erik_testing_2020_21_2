﻿using OE_PAGE_OBJECT_BEADANDO.Models;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OE_PAGE_OBJECT_BEADANDO.Widgets
{
    class SearchResultWidget : BasePage
    {
        public SearchResultWidget(IWebDriver webDriver) : base(webDriver)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(20));
            wait.Until(d => d.FindElement(By.CssSelector("ul[class = 'box-flex vertical-switcher fd-row']")));
        }

        public IWebElement OrderSwitcher => Driver.FindElement(By.CssSelector("ul[class = 'box-flex vertical-switcher fd-row']"));
        public bool CheckOrderType(OrderType type)
        {

            switch (type)
            {
                case OrderType.Delivery:
                    return OrderSwitcher.FindElements(By.CssSelector("li[class='vertical-switcher-restaurants ic-vert-restaurants active']")).Count > 0;
                case OrderType.Pickup:
                    return OrderSwitcher.FindElements(By.CssSelector("li[class='vertical-switcher-pickup ic-vert-pickup active']")).Count > 0;
                default:
                    break;
            }
            return false;
        }

        public bool CheckLocation(string location)
        {
            var element = Driver.FindElement(By.CssSelector("span[class='header-order-button-content']"));
            return location == element.Text;
        }
    }
}
