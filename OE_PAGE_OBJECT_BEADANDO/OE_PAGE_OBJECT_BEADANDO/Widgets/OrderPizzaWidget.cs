﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OE_PAGE_OBJECT_BEADANDO.Widgets
{
    class OrderPizzaWidget : BasePage
    {
        public OrderPizzaWidget(IWebDriver webDriver) : base(webDriver)
        {
        }

        public IWebElement GetPizza(string name)
        {
            return Driver.FindElement(By.CssSelector($"button[aria-label = '{name}']"));
        }

        public OrderPizzaResultWidget ClickOnPizza(string name)
        {
            var pizza = this.GetPizza(name);
            pizza.Click();
            return new OrderPizzaResultWidget(Driver);
        }
    }
}
