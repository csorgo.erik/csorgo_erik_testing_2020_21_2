﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OE_PAGE_OBJECT_BEADANDO.Widgets
{
    class OrderDetailsWidget : BasePage
    {
        public OrderDetailsWidget(IWebDriver webDriver) : base(webDriver)
        {
            
        }

        public IWebElement SubmitButton => Driver.FindElement(By.CssSelector("button[class = 'button product-add-to-cart-button full']"));

        public OrderPizzaResultWidget FinalizeOrder()
        {
            SubmitButton.Click();
            return new OrderPizzaResultWidget(Driver);
        }
    }

}
