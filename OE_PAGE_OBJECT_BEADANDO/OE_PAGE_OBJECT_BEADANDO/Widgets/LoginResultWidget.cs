﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OE_PAGE_OBJECT_BEADANDO.Widgets
{
    class LoginResultWidget : BasePage
    {
        public LoginResultWidget(IWebDriver webDriver) : base(webDriver)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(20));
            wait.Until(d => d.FindElement(By.CssSelector("input[id='delivery-information-postal-index']")));
        }

        public bool IsNameVisible(string name)
        {
            IWebElement nameElement = Driver.FindElement(By.CssSelector("span[class='tooltip-reorder login-label']"));
            return nameElement.Text.ToLower() == name.ToLower();
        }

        public bool IsTokenCreated()
        {
            var cookie = Driver.Manage().Cookies.GetCookieNamed("refresh_token");
            return cookie != null;
        }
    }
}
