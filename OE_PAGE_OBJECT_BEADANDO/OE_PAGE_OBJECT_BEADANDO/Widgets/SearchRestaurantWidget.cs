﻿using OE_PAGE_OBJECT_BEADANDO.Models;
using OE_PAGE_OBJECT_BEADANDO.Pages;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OE_PAGE_OBJECT_BEADANDO.Widgets
{
    class SearchRestaurantWidget : BasePage
    {
        public SearchRestaurantWidget(IWebDriver webDriver) : base(webDriver)
        {
        }

        public IWebElement LocationInput => Driver.FindElement(By.CssSelector("input[id='delivery-information-postal-index']"));
        public IWebElement DeliveryButton => Driver.FindElement(By.CssSelector("button[data-testid='delivery_button']"));
        public IWebElement PickupButton => Driver.FindElement(By.CssSelector("button[data-testid='pickup_button']"));

        public void SetLocation(string location)
        {
            LocationInput.SendKeys(location);
        }

        public SearchResultPage ClickButton(OrderType type)
        {
            switch (type)
            {
                case OrderType.Delivery:
                    this.DeliveryButton.Click();
                    break;
                case OrderType.Pickup:
                    this.PickupButton.Click();
                    break;
                default:
                    break;
            }

            return new SearchResultPage(Driver);
        }

        public SearchResultPage SearchRestaurant(SearchRestaurantModel model)
        {
            this.SetLocation(model.Location);
            return ClickButton(model.OrderType);
        }


    }
}
