﻿using OE_PAGE_OBJECT_BEADANDO.Models;
using OE_PAGE_OBJECT_BEADANDO.Pages;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OE_PAGE_OBJECT_BEADANDO.Widgets
{
    class LoginWidget : BasePage
    {
        public LoginWidget(IWebDriver webDriver) : base(webDriver)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(20));
            wait.Until(d => d.FindElement(By.Id("_password")));
        }

        public IWebElement Email => Driver.FindElement(By.Id("_username"));
        public IWebElement Password => Driver.FindElement(By.Id("_password"));
        public IWebElement LoginButton => Driver.FindElement(By.CssSelector("button[type = 'submit']"));

        public void SetEmail(string email)
        {
            this.Email.SendKeys(email);
        }

        public void SetPassword(string password)
        {
            this.Password.SendKeys(password);
        }

        public LoginResultPage ClickLogin(LoginModel model)
        {
            this.SetEmail(model.Email);
            this.SetPassword(model.Password);
            this.LoginButton.Click();
            return new LoginResultPage(Driver);
        }
    }
}
