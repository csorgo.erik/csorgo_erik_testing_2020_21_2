﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OE_PAGE_OBJECT_BEADANDO.Widgets
{
    class OrderPizzaResultWidget : BasePage
    {
        public OrderPizzaResultWidget(IWebDriver webDriver) : base(webDriver)
        {
        }

        public IWebElement Cart => Driver.FindElement(By.CssSelector("div[class = 'cart-summary-items']"));

        public bool CheckCart(string name)
        {
            var orders = Cart.FindElements(By.XPath($"//*[contains(text(), '{name}')]"));
            return orders.Count > 0;
        }
    }
}
