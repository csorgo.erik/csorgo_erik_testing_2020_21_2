﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Assignment.Numbers;
using Assignment.Strings;
using Moq;

namespace AssignmentTests
{
    public class AssignmentTests
    {
        NumberGenerator numGen;
        Mock<INumberGenerator> mockNumberGenerator;
        NumberUtils numUtils;
        StringGenerator strGen;
        StringUtil strUtil;

        [SetUp]
        public void setup()
        {
            numGen = new NumberGenerator();
            numUtils = new NumberUtils();
            mockNumberGenerator = new Mock<INumberGenerator>();
            strGen = new StringGenerator(mockNumberGenerator.Object);
            strUtil = new StringUtil();

            mockNumberGenerator.Setup(x => x.GenerateEven(0)).Returns(0);
            mockNumberGenerator.Setup(x => x.GenerateEven(-1)).Throws(new ArgumentOutOfRangeException());

            mockNumberGenerator.Setup(x => x.GenerateOdd(1)).Returns(1);
            mockNumberGenerator.Setup(x => x.GenerateOdd(0)).Throws(new ArgumentOutOfRangeException());
        }

        [TestCase(-130)]
        [TestCase(-1)]
        [TestCase(-2)]
        public void GenerateEven_Should_ThrowArgumentOutOfRangeException_When_LimitIsNegative(int limit)
        {
            //Act & Assert
            Assert.Throws(typeof(ArgumentOutOfRangeException), () => numGen.GenerateEven(limit));
        }

        [TestCase(2)]
        [TestCase(3)]
        [TestCase(130)]
        public void GenerateEven_Should_ReturnPositiveEvenNumber_When_LimitIsGreaterThanZero(int limit)
        {
            //Act
            int result = numGen.GenerateEven(limit);

            //Assert
            Assert.That(result <= limit && result >= 0);
            Assert.That(result % 2 == 0);
        }

        [TestCase(2)]
        [TestCase(3)]
        [TestCase(130)]
        public void GenerateOdd_Should_ReturnPositiveOddNumber_When_LimitIsGreaterThanOne(int limit)
        {
            //Act
            int result = numGen.GenerateOdd(limit);

            //Assert
            Assert.That(result <= limit && result >= 1);
            Assert.That(result % 2 == 1);
        }

        [TestCase(0)]
        [TestCase(-1)]
        [TestCase(-2)]
        public void GenerateOdd_Should_ThrowArgumentOutOfRangeException_When_LimitIsZeroOrNegative(int limit)
        {
            //Act & Assert
            Assert.Throws(typeof(ArgumentOutOfRangeException), () => numGen.GenerateOdd(limit));
        }

        [TestCase(2)]
        [TestCase(3)]
        [TestCase(130)]
        [TestCase(-50)]
        public void GetDivisors_Should_ReturnAllOfTheDivisors_When_InputIsValid(int number)
        {
            //Act
            var result = numUtils.GetDivisors(number);

            //Assert
            Assert.GreaterOrEqual(result.Count, 2);
        }

        [Test]
        public void GetDivisors_Should_ReturnListWithOnlyOneElement_When_InputIsOne()
        {
            var result = numUtils.GetDivisors(1);

            //Assert
            Assert.GreaterOrEqual(result.Count, 1);
        }

        [TestCase(2)]
        [TestCase(3)]
        [TestCase(7)]
        [TestCase(13)]
        public void IsPrime_Should_ReturnTrue_When_InputIsPrime(int number)
        {
            //Act
            bool result = numUtils.IsPrime(number);

            //Assert
            Assert.IsTrue(result);
        }

        [TestCase(0)]
        [TestCase(-1)]
        [TestCase(1)]
        [TestCase(12)]
        [TestCase(240)]
        public void IsPrime_Should_ReturnFalse_When_InputIsNotPrime(int number)
        {
            //Act
            bool result = numUtils.IsPrime(number);

            //Assert
            Assert.IsFalse(result);
        }

        [TestCase(0)]
        [TestCase(2)]
        [TestCase(-12)]
        [TestCase(240)]
        public void EvenOrOdd_Should_ReturnEven_When_InputIsEven(int number)
        {
            //Act
            string result = numUtils.EvenOrOdd(number);

            //Assert
            Assert.AreEqual("even", result);
        }

        [TestCase(1)]
        [TestCase(3)]
        [TestCase(-11)]
        [TestCase(253)]
        public void EvenOrOdd_Should_ReturnOdd_When_InputIsOdd(int number)
        {
            //Act
            string result = numUtils.EvenOrOdd(number);

            //Assert
            Assert.AreEqual("odd", result);
        }

        [TestCase("Racecar")]
        [TestCase("Lepers repel")]
        [TestCase("Never a foot too far, even.")]
        public void IsPalindrom_Should_ReturnTrue_When_InputIsPalindrom(string str)
        {
            //Act
            var result = strUtil.IsPalindrom(str);

            //Assert
            Assert.IsTrue(result);
        }

        [TestCase("")]
        [TestCase("Lepersdsad repel")]
        [TestCase("Never a foot too far.")]
        public void IsPalindrom_Should_ReturnFalse_When_InputIsNotPalindrom(string str)
        {
            //Act
            var result = strUtil.IsPalindrom(str);

            //Assert
            Assert.IsFalse(result);
        }

        [Test]
        public void GenerateEvenOddPairs_Should_ReturnAnEmptyList_When_PairCountIsZero()
        {
            //Act
            var result = strGen.GenerateEvenOddPairs(0, 5);

            //Assert

            mockNumberGenerator.Verify(x => x.GenerateEven(It.IsAny<int>()), Times.Never);
            mockNumberGenerator.Verify(x => x.GenerateOdd(It.IsAny<int>()), Times.Never);

            Assert.That(result.Count == 0);
        }

        [TestCase(1, 5)]
        [TestCase(5, 9)]
        public void GenerateEvenOddPairs_Should_ReturnListWithElements_When_InputIsValid(int pairCount, int max)
        {
            //Arrange
            mockNumberGenerator.Setup(x => x.GenerateEven(max)).Returns(0);
            mockNumberGenerator.Setup(x => x.GenerateOdd(max)).Returns(1);

            //Act
            var result = strGen.GenerateEvenOddPairs(pairCount, max);

            //Assert

            mockNumberGenerator.Verify(x => x.GenerateEven(It.IsAny<int>()), Times.Exactly(pairCount));
            mockNumberGenerator.Verify(x => x.GenerateOdd(It.IsAny<int>()), Times.Exactly(pairCount));

            Assert.That(result.Count == pairCount);
            Assert.That(result[0] == "0,1");
        }

        [TestCase(1, 0)]
        [TestCase(5, -1)]
        public void GenerateEvenOddPairs_Should_ThrowArgumentOutOfRangeException_When_LimitIsInvalid(int pairCount, int max)
        {
            Assert.Throws(typeof(ArgumentOutOfRangeException), () => strGen.GenerateEvenOddPairs(pairCount,max));

            if(max == 0)
            {
                mockNumberGenerator.Verify(x => x.GenerateEven(It.IsAny<int>()), Times.Once);
                mockNumberGenerator.Verify(x => x.GenerateOdd(It.IsAny<int>()), Times.Once);
            }
            else if (max == -1)
            {
                mockNumberGenerator.Verify(x => x.GenerateEven(It.IsAny<int>()), Times.Once);
                mockNumberGenerator.Verify(x => x.GenerateOdd(It.IsAny<int>()), Times.Never);
            }
        }
    }
}
