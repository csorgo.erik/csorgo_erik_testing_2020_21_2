﻿using System.Linq;
using System.Text;

/**
 * https://hu.wikipedia.org/wiki/Palindrom
 */
namespace Assignment.Strings
{
    public class StringUtil
    {
        /// <summary>
        /// Checks if the string passed in is a palindrom or not.
        /// </summary>
        /// <param name="str"></param>
        /// <returns>True if the passed in string is a palindrom, otherwise false.</returns>
        public bool IsPalindrom(string str)
        {
            //var reverse = new StringBuilder(str).ToString().Reverse<char>().ToString();
            //return str.Equals(str);

            var strFormatted = str.Replace(" ", string.Empty).Replace(",", string.Empty).Replace(".", string.Empty).ToLower().ToString();
            var reverse = new string(strFormatted.Reverse().ToArray()).ToString();

            return str == string.Empty ? false : strFormatted.Equals(reverse.ToString());
        }
    }
}

